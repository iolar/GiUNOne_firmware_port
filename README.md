	GiUNOne_firmware_port
	Arduino UNO ATmega328p firmware
--------------------------------------------------------------------------------------------------
Sviluppo del firmware per la gestione dell'architettura dell'Arduino UNO con processore ATmega328p.<br>
In particolare sono state sviluppate strutture e funzioni per la gestione di:
1. pin digitali;
2. pin analogici;
3. convertitore Analogico-Digitale interno;
4. tutti e tre i timers interni;
5. delay;
6. EEPROM;
7. UART;
8. generazione di PWM;
9. un paio di encoders;

10. Sono presenti inoltre dei test e delle DEMOs illustrative del funzionamento del firmware.

______________________________________________________________________________________________________

1. <b>PIN DIGITALI</b>: <br>
	pins.h digio.h<br>
Caratterizzati da una struct che contiene i tre registri descrittivi <br>
delle funzionalità dei pin digitali, ovvero (pins.h):<br>
	-   Registro direzionale DDRx;<br>
	-   Registro di porta PORTx;<br>
	-   Registro di lettura ingresso ingresso PINx;<br>
	-   bit rispettivo nella porta;<br><br>
Contiene inoltre i registri per la gestione della PWM, ove possibile:<br>
	-   Timer/Counter Control Register TCCR;<br>
	-   Output Compare Register OCR;<br>
	-   Una maschera di selezione;<br><br>
Funzioni per la gestione in digio.h:<br>
	-   DigIO_init, Inizializzazione di tutti i pin digitali;<br>
	-   DigIO_numChannels, Return del numero di canali digitali;<br>
	-   DigIO_setDirection, DigIO_getDirection, Set e Get per la direzione;<br>
	-   DigIO_setValue, DigIO_getValue, Set e Get per il valore;<br>
<br>

2. <b>PIN ANALOGICI</b> &... 
3. ...<b>ADC</b> (Convertitore Analogico Digitale) <br>
	apins.h analog.h<br>
I pin analogici sono utilizzati solo come input.<br>
Caratterizzati da una struct che contiene i tre registri descrittivi <br>
delle funzionalità dei pin analogici, ovvero (apins.h):<br>
	-  Registro direzionale DDRx;<br>
	-   Registro di porta PORTx;<br>
	-   Registro di ingresso PINx;<br>
	-   bit rispettivo nella porta;<br><br>
Contiene inoltre:<br>
	-   una maschera di selezione per il multiplexer dell'ADC.<br><br>
Funzioni per la gestione dei pin analogici e dell'ADC in analog.h:<br>
	-   ADC_init, Inizializzazione del Convertitore Analogico Digitale:<br>
	   -   con Prescaler 128, quindi 125 kHz di clock source,<br>
	   -   AVcc +5V come voltaggio di riferimento;<br>
	-   Analog_init, Inizializzazione dei pin analogici;<br>
	-   Analog_numChannels, Return del numero di canali analogici;<br>
	-   read_ADC, Lettura dell'ADC su un determinato canale analogico;<br>
	-   ADC_end, Disabilitazione ADC (Risparmio energetico);<br>
<br>

4. <b>TIMERS</b>:<br>
	timer.h<br>
Un timer è definito come una struct che descrive:<br>
	-   Il numero del timer;<br>
	-   Il prescaler che si vuole utilizzare.<br>
	    -   NOTA: i prescalers sono definiti per ogni timer nel file "prescalers.h";<br>
	-  La frequenza Hz (usata per i timer 8-bit, timer_0,timer_2);<br>
	-   Durata in ms del tick (usata per il timer a 16-bit, timer_1);<br>
	-   Il valore dell'Output Compare Register:<br>
	   -   compare_match_register = [Fclk / (prescaler * desired_int_freq)] - 1;<br>
	-   La funzione da svolgere ogni matching del timer;<br>
	-   argomenti da passare alla funzione suddetta;<br><br>
E' presente un vettore di grandezza "numero dei timers",<br>
che ha al suo interno per ogni indice la struttura del rispettivo timer.<br><br>
Funzioni:<br>
	-   Timer_init, Inizializzazione delle strutture per i timers;<br>
	-   Timer_create, Creazione, riempimento delle strutture per i timers;<br>
	-   Timer_stop, Stop di un timer (Lavora sui registri);<br>
	-   Timer_destroy, Distruzione di un timer (lavora sulle strutture, ma richiama Timer_stop);<br>
	-   Timer_start, Start di un timer, richiama la rispettiva funzione start del timer richiesto. Quest'ultima agisce sui registri.<br>
	-   ISR (Interrupt Service Routine) per ogni timer.<br><br>
  ATTENZIONE: <br>
	Il codice è tale per cui se un timer già avviato (start)<br>
	lo si vuole utilizzare per un PWM, ciò non è possibile.<br>
	Similmente il viceversa:<br>
	un timer già avviato in modalità PWM, non potrà essere utilizzato<br>
	come semplice counter.<br>
	La funzione di start ritornerà un errore "TimerForPWM" = -1,<br>
	che indica che per utilizzare questo timer come semplice counter<br>
	bisogna prima distaccarlo dalla modalità di PWM.<br>
	Questi controlli vengono effettuali grazie a due array di flags<br>
	dichiarati nel file "flags.h".<br><br>
  NOTA:<br>
// ArduinoUNO, ATmega 328p timers: timer 0, timer 1, timer 2.<br>
// timer 0 is used on pin 5 and on pin 6.   It is 8 bit, with PWM.<br>
// timer 1 is used on pin 9 and on pin 10.  It is 16 bit, with PWM.<br>
// timer 2 is used on pin 11 and on pin 3.  It is 8 bit, with PWM and<br>
//                                       with Asynchronous Operations.<br>
<br>

5. <b>DELAY</b>: <br>
	delay.h<br>
Basato sull'inclusione di "util_delay.h"<br>
<br>

6. <b>EEPROM</b>:<br>
	eeprom.h<br>
EEPROM da 1KB (indirizzi da 0 a 1023);<br>
Ha una durata di massimo 100000 cicli di scrittura/cancellazione,<br>
	da usare quindi in modo saggio, per esempio solo per dati di configurazione.<br><br>
Funzioni per la gestione:<br>
	-   EEPROM_init, inizializza la EEPROM nella modalità "Erase and Write in one operation (Atomic Operation)";<br>
	-   EEPROM_read, legge dati dalla eeprom in una data posizione;<br>
	-   EEPROM_write, scrive dati nella eeprom in una data posizione;<br><br>
ATTENZIONE:<br>
	-   L'implementazione di queste funzioni NON è buona nel caso in cui nel software sia presente un flash boot loader;<br>
	-   L'implementazione è robusta nel caso di accesso alla EEPROM da interrupt.<br>
<br>

7. <b>UART</b>:<br>
	uart.h<br>
La UART è gestita come una struttura contenente:<br>
	-   un buffer TX di trasmissione;<br>
	   -   inizio tx; 
	   -   fine tx;<br>
	   -   grandezza tx;<br>
	-   un buffer RX di ricezione;<br>
	   -   inizio rx;<br>
	   -   fine rx;<br>
	   -   grandezza rx;<br>
	-   Baud Rate;<br>
	-   numero della uart;<br><br>
Funzioni per la gestione:<br>
	-   UART_init, inizializza una uart ad un certo baud rate;<br>
	-   UART_putChar, inserisce un carattere nel buffer di trasmissione;<br>
	-   UART_getChar, legge un carattere dal buffer di ricezione;<br>
	-   UART_txBufferSize, ritorna la grandezza del buffer di trasmissione;<br>
	-   UART_rxBufferSize, ritorna la grandezza del buffer di ricezione;<br>
	-   UART_txBufferFull, ritorna il n° di caratteri da trasmettere presenti nel buffer di trasmissione;<br>
	-   UART_rxBufferFull, ritorna il n° di caratteri da leggere presenti nel buffer di ricezione;<br>
	-   UART_txBufferFree, ritorna il n° di spazio disponibile nel buffer per la trasmissione;<br>
	-   ISR;<br><br>
"uart.c" fa riferimento ad un altro file "buffer_utils.h":<br>
	in questo file sono definiti due buffer circolari, uno per la trasmissione e uno per la ricezione.<br>
<br>

8. <b>PWM</b>:<br>
	pwm.h<br>
Per la Pulse Width Modulation è stata definita una enumerazione di errori<br>
e un paio di tipi di PWM: <b>Fast</b> e <b>PhaseCorrect</b> (migliore per la gestione di motori).<br><br>
Le funzioni per la gestione sono (pwm.c):<br>
	-   PWM_init, inizializza un timer per la modalità PWM, con un tipo di PWM e un prescaler.<br>
	  -   NOTA: i prescalers sono definiti per ogni timer nel file "prescalers.h";<br>
	-   PWM_numChannels, ritorna il numero dei PIN, tutti i digitali, non solo PWM;<br>
	-   PWM_getDutyCycle, ritorna il duty cycle presente su uno specifico canale;<br>
	-   PWM_setDutyCycle, setta il duty cycle per uno specifico canale;<br>
	-   PWM_enable, abilita o disabilita la PWM per un canale;<br>
	-   PWM_isEnabled, controllo se PWM abilitata o meno su un canale;<br>
	-   PWM_detach, distacca un timer dalla modalità PWM, per utilizzarlo, per esempio, come contatore.<br><br>
ATTENZIONE: <br>
	Il codice è tale per cui un timer già avviato in modalità PWM, <br>
	non potrà essere utilizzato come semplice counter.<br>
	Similmente il viceversa:<br>
	se un timer già avviato (start) lo si vuole utilizzare per una PWM, ciò non è possibile.<br>
	La funzione di start ritornerà un errore "TimerActive" = -4,<br>
	che indica che per utilizzare questo timer in modalità PWM,<br>
	bisogna prima fermare e distruggere il timer.<br>
	Questi controlli vengono effettuali grazie a due array di flags<br>
	dichiarati nel file "flags.h".<br><br>
NOTA:<br>
	-   Fpwm = Fclk/(Prescaler*256);<br>
	-   Il codice non è pensato per realizzare una modulazione in frequenza;<br>
	-   Modalità: non invertente.<br>
<br>

9. <b>ENCODERS</b>:<br>
	encoder.h<br>
Un encoder è definito tramite una struttura che contiene i seguenti campi:<br>
	-   valore corrente;<br>
	-   valore campionato;<br>
	-   stato del pin;<br><br>
Viene poi utilizzato un array di encoders che ne contiene due.<br>
L'encoder 0 è associato ai pin della porta B: pin 12 e pin 8;<br>
L'encoder 1 è associato ai pin della porta D: pin 7 e pin 4;<br><br>

Funzioni per l'utilizzo di encoders:<br>
	-   Encoder_init, inizializzazione dei pin e delle maschere per gli encoders;<br>
	-   Encoder_sample, campiona il valore corrente;<br>
	-   Encoder_numEncoders, ritorna il numero di encoders utilizzabili;<br>
	-   Encoder_getValue, ritorna il valore campionato dalla funzione _sample;<br>
	-   Encoder_end, termina l'acquisizione e l'elaborazione dei valori per l'encoder;<br>
	-   ISR, elabora il dato acquisito ad ogni interrupt-on-change;<br><br>
Inoltre:<br>
	Viene utilizzata una tabella di transizione per l'elaborazione/decodifica.<br>
Nota:<br>
	La tabella e il metodo dell'ISR, vanno modificati in base all'uso dell'encoder.<br>
<br>

<b>10.</b><br>
    <b>TEST</b>:<br>
	-   Test vari sulle varie parti dell'architettura;<br>
	-   Test sui packet: comunicazione a pacchetti tramite UART;<br>
    <b>DEMOs</b>:<br>
	-   IRrecv: ricezione segnale da sensore IR [da rivedere];<br>
	-   arch_demo.c: Demo per provare il funzionamento di Encoder, PWM, ADC. Necessita di un potenziometro, di 6 led, resistenze e un paio di pulsanti.<br>

______________________________________________________________________________________________________
<b>PER MAGGIORI INFORMAZIONI FARE RIFERIMENTO AI COMMENTI NEL CODICE E AL DATASHEET DEL MICROCONTROLLORE.<br>
Buon utilizzo! :)</b> <br>

	Giulia G.

