#include <stdint.h>

//Used to know which timer is in use as simple timer
uint8_t Active_Timers[3];

//Used to know wich timer is in use for PWM mode
uint8_t ActivePWM[3];
