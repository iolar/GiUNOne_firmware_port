#include "analog.h"
#include "apins.h"
#include <avr/io.h>
#include <avr/pgmspace.h>
#define ANALOG_NUM_CHANNELS 6

void ADC_init(void){
    //initializes ADC
    // . Prescaler at 128, so 125KHz clock source
    // . AVcc (+5V) as voltage reference
    
    ADCSRA |= ((1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0));
    
    ADMUX |= (1<<REFS0);
    ADMUX &= ~(1<<REFS1);
    
    ADCSRA |= (1<<ADEN);    //ADC Enable
    ADCSRA |= (1<<ADSC);    //ADC Start Conversion
}

//initializes the analog pins of the chip
void Analog_init(void){
    // we set all analog pins as input
    uint8_t num_channels = Analog_numChannels();
    int i = 0;
    for(i = 0; i < num_channels; i++){
        const aPin* mapping = apins + i;
        uint8_t mask = 1 << mapping->bit;
        *(mapping->dir_register) &= ~mask;
    }
    return;
}

// returns the number of analog pins on the chip
uint8_t Analog_numChannels(void){
    return ANALOG_PINS_NUM; // fixed maximum number of analog pins
                            // mapped to arduino
}

// apin:
// A0 = 0; A1 = 1; A2 = 2; A3 = 3; A4 = 4; A5 = 5;
uint16_t read_ADC(uint8_t apin){
    
    if(apin >= ANALOG_PINS_NUM) { return 0;}
    
    const aPin* mapping = apins + apin;
    uint8_t channel = mapping->mux_mask;
    
    ADMUX &= 0xF0;      //Clears the older read channel
    ADMUX |= channel;   //Sets the new channel to read
    
    ADCSRA |= (1 << ADSC); //Starts the new conversion
    while(ADCSRA & (1 << ADSC)); //Waits until the conversion is done
                                //then ADSC will turn to zero
    return ADCW;
}   

// Useful for example to reduce energy consumption
// when the ADC is not used
void ADC_end(){
    ADCSRA &= ~(1<<ADEN);    //ADC Disable
}
