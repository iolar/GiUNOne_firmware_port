#include "apins.h"
#include "analog.h"
#include <avr/io.h>

const aPin apins[] =
    {
        //A0
        {
            .in_register = &PINC,
            .out_register = &PORTC,
            .dir_register = &DDRC,
            .bit = 0,
            .mux_mask = 0
        },
        //A1
        {
            .in_register = &PINC,
            .out_register = &PORTC,
            .dir_register = &DDRC,
            .bit = 1,
            .mux_mask = (1 << MUX0)
        },
        //A2
        {
            .in_register = &PINC,
            .out_register = &PORTC,
            .dir_register = &DDRC,
            .bit = 2,
            .mux_mask = (1 << MUX1)
        },
        //A3
        {
            .in_register = &PINC,
            .out_register = &PORTC,
            .dir_register = &DDRC,
            .bit = 3,
            .mux_mask = (1 << MUX0) | (1 << MUX1)
        },
        //A4
        {
            .in_register = &PINC,
            .out_register = &PORTC,
            .dir_register = &DDRC,
            .bit = 4,
            .mux_mask = (1 << MUX2)
        },
        //A5
        {
            .in_register = &PINC,
            .out_register = &PORTC,
            .dir_register = &DDRC,
            .bit = 5,
            .mux_mask = (1 << MUX0) | (1 << MUX2)
        }
    };
