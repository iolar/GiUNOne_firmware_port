#include "eeprom.h"
#include <avr/interrupt.h>
#include <avr/io.h>

// initializes EEPROM to 
// "Erase and Write in one operation (Atomic Operation)",
// waiting for EEPE not set (erase/write in progress),
// otherwise any write on EEPMn will be ignored.
void EEPROM_init(void){
    cli();
    while(EECR & (1 << EEPE));
    EECR &= ~(1 << EEPM1) & ~(1 << EEPM0);
    sei();
}

void EEPROM_read(void* dest_, const uint16_t src, uint16_t size){
  uint8_t * dest=(uint8_t*)dest_;
  uint16_t s=src;
  uint16_t s_end=src+size;
  
  //check if address "s" is less than 1024
  //because with uint16_t it could arrive to 2^16,
  //but EEPROM bytes are only 1024
  //and couldn't use uint8_t, 'cause it could address only till 256.
  if(!(s < 1024) || !(s_end <= 1024)){return;}
  
  while(s<s_end){
    cli();
    while(EECR & (1 << EEPE));
    EEAR = s;
    EECR |= (1 << EERE);
    *dest = EEDR;
    sei();
    ++s;
    ++dest;
  }
}

void EEPROM_write(uint16_t dest, const void* src_,  uint16_t size){
  const uint8_t * s=(uint8_t*)src_;
  const uint8_t * s_end=s+size;
  
  //check if address "dest" is less than 1024
  //because with uint16_t it could arrive to 2^16,
  //but EEPROM bytes are only 1024
  //and couldn't use uint8_t, 'cause it could address only till 256.
  if(!(dest < 1024)){return;}
  
  while(s<s_end){
    cli();
    while(EECR & (1 << EEPE));
    EEAR = dest;
    EEDR = *s;
    EECR |= 1 << EEMPE;
    EECR |= 1 << EEPE;
    sei();
    ++s;
    ++dest;
  }
}
