#include "encoder.h"
#include <avr/io.h>
#include <avr/interrupt.h>

#define NUM_ENCODERS 2
#define ENCODER_MASK_B 0x11 //bit 0 (pin 8) & bit 4 (pin 12) of PORT B
                            //used for encoders
#define ENCODER_MASK_D 0x90 //bit 7 (pin 7) & bit 4 (pin 4) of PORT D
                            //used for encoders
                            
typedef struct{
  uint16_t current_value;
  uint16_t sampled_value;
  uint8_t  pin_state;
}  Encoder;

Encoder _encoders[NUM_ENCODERS]={
  {0,0},
  {0,0}
};

//Initializes the encoder subsystem
void Encoder_init(void){
    cli();
    DDRB &= ~ENCODER_MASK_B;//set encoder pins of PORT B as input
    DDRD &= ~ENCODER_MASK_D;//set encoder pins of PORT D as input
    PORTB |= ENCODER_MASK_B;//enable pull-up resistors PORT B
    PORTD |= ENCODER_MASK_D;//enable pull-up resistors PORT D
    PCICR |= (1 << PCIE2)|(1 << PCIE0);//set interrupt on change,
                                //looking up PCMSK0 and PCMSK2
    PCMSK0 |= ENCODER_MASK_B;//set PCINT0 
                    //to trigger an interrupt on state change
    PCMSK2 |= ENCODER_MASK_D;//set PCINT2
                    //to trigger an interrupt on state change
    sei();
}

// samples the encoders, saving the respective values in a temporary storage
void Encoder_sample(void){
  cli();
  for (uint8_t i=0; i<NUM_ENCODERS; ++i)
    _encoders[i].sampled_value=_encoders[i].current_value;
  sei();
}

// returns the number of the encoder 
uint8_t Encoder_numEncoders(void){
  return NUM_ENCODERS;
}

// returns the value of an encoder, when sampled with the Encoder_sample();
uint16_t Encoder_getValue(uint8_t num_encoder){
  return _encoders[num_encoder].sampled_value;
}

static const int8_t _transition_table []=
  {
      0,  //0000
     -1, //0001
      1,  //0010
      0,  //0011
      1,  //0100
      0,  //0101
      0,  //0110
     -1, //0111
     -1, //1000
      0,  //1001
      0,  //1010
      1,  //1011
      0,  //1100
      1,  //1101
     -1,  //1110
      0   //1111
    };

// terminates the encoder subsystem
void Encoder_end(void){
    cli();
    PCMSK0 &= ~ENCODER_MASK_B;
    PCMSK2 &= ~ENCODER_MASK_D;
    sei();
}

ISR(PCINT0_vect){
    cli();
    char portB_value = PINB&ENCODER_MASK_B; //pin 12 & pin 8
    
    //encoder 0 (associated to port B)
    if(portB_value == ENCODER_MASK_B)     { portB_value = 0x03;}
    else if(portB_value == 0x10)          { portB_value = 0x02;}
    
    uint8_t new_pin_state = portB_value;
    uint8_t idx=(_encoders[0].pin_state<<2)| new_pin_state ;
    _encoders[0].current_value+=_transition_table[idx];
    _encoders[0].pin_state=new_pin_state;
    
    sei();
}

ISR(PCINT2_vect){
    cli();
    char portD_value = PIND&ENCODER_MASK_D; //pin 7 & pin 4
    
    //encoder 1 (associated to port D)
    if(portD_value == ENCODER_MASK_D)     { portD_value = 0x03;}
    else if(portD_value == 0x80)          { portD_value = 0x02;}
    else if(portD_value == 0x10)          { portD_value = 0x01;}
    
    uint8_t new_pin_state = portD_value;
    uint8_t idx=(_encoders[1].pin_state<<2)| new_pin_state ;
    _encoders[1].current_value+=_transition_table[idx];
    _encoders[1].pin_state=new_pin_state;
    
    sei();
}

/*
ISR(PCINT2_vect, ISR_ALIASOF(PCINT0_vect));
//Note that the ISR_ALIASOF() feature requires GCC 4.2 or above
//(or a patched version of GCC 4.1.x)
//deprecated: ISR_ALIAS(PCINT2_vect, PCINT0_vect);
//this is compatible for all versions of GCC 
//rather than just GCC version 4.2 onwards
*/
