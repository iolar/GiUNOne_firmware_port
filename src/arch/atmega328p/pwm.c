#include "pwm.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <string.h>
#include "pins.h"

#include <flags.h>
extern uint8_t Active_Timers[3];
uint8_t ActivePWM[3] = {0,0,0};

// -----------------------------------------------------//
// WARNING: Don't use active timers for PWM
//  and their related pins!
// Otherwise functions will return you an error.
// -----------------------------------------------------//

// initializes a timer for PWM mode
// and sets the PWM period to something reasonable
PWMError PWM_init(char* timer, PWMtype type, uint8_t prescaler){
    cli();
    
    // Setup info:
    //Fpwm = Fclk = 16MHz
    // Info: Fpwm = Fclk/(Prescaler*256)
    
    //8-bit timers
    //WGM TOP = 0xFF (so not OCR, WGMn2 is never = 1)
    //So no frequency modulation!
    
    //16-bit timers
    //WGM TOP = 0x00FF (so not OCR)
    //using only 8-bit
    //No frequency modulation!
    
    //Using non-inverting mode for both Fast and PhaseCorrect PWM
    
    if(!strcmp(timer, "timer_0")){
        if(Active_Timers[0] == 1) { sei(); return TimerActive;}
        ActivePWM[0] = 1;
        
        TIMSK0 = 0;
        //8-bit timer
        TCCR0A = 0;
        TCCR0B = 0;
        if(!type){ TCCR0A = (1<<WGM01);}
        TCCR0A |= (1<<WGM00);
        TCCR0B = prescaler;
    }
    
    else if(!strcmp(timer, "timer_1")){
        if(Active_Timers[1] == 1) { sei(); return TimerActive;}
        ActivePWM[1] = 1;
        
        TIMSK1 = 0;
        //16-bit timer
        TCCR1A = 0;
        TCCR1B = 0;
        if(!type) { TCCR1B = (1<<WGM12);}
        TCCR1A = (1<<WGM10);
        TCCR1B |= prescaler; 
        
        //clearing higher byte of Output Compare Register 16-bit
        //to use only the lower byte (8-bit)
        OCR1AH = 0;
        OCR1BH = 0;
    }
    
    else if(!strcmp(timer, "timer_2")){
        if(Active_Timers[2] == 1) { sei(); return TimerActive;}
        ActivePWM[2] = 1;
        
        TIMSK2 = 0;
        //8-bit timer
        TCCR2A = 0;
        TCCR2B = 0;
        if(!type) { TCCR2A = (1<<WGM21);}
        TCCR2A |= (1<<WGM20); 
        TCCR2B = prescaler;
    }
    
    else { sei(); return PWMChannelOutOfBound;}
    
    sei();
    return PWMSuccess;
}

uint8_t PWM_numChannels(void){
    return PINS_NUM;
}

// what was the duty cycle I last set?
uint8_t PWM_getDutyCycle(uint8_t c){
    
    if( (((c == 5) | (c == 6))  & (Active_Timers[0] == 1)) |
        (((c == 9) | (c == 10)) & (Active_Timers[1] == 1)) |
        (((c == 11)| (c == 3))  & (Active_Timers[2] == 1))   ){ 
        return TimerActive;
    }
        
    if (c>=PINS_NUM)
        return PWMChannelOutOfBound;
    const Pin* pin = pins+c;
    if (!pin->tcc_register)
        return PWMChannelOutOfBound;
    return 255-*pin->oc_register;//compensate for the inverting setting,
                                //makes non-inverting
}

// sets the duty cycle
PWMError PWM_setDutyCycle(uint8_t c, uint8_t duty_cycle){
    
    if( (((c == 5) | (c == 6))  & (Active_Timers[0] == 1)) |
        (((c == 9) | (c == 10)) & (Active_Timers[1] == 1)) |
        (((c == 11)| (c == 3))  & (Active_Timers[2] == 1))   ){ 
        return TimerActive;
    }
    
    if (c>=PINS_NUM)
        return PWMChannelOutOfBound;
    const Pin* pin = pins+c;
    if (!pin->tcc_register)
        return PWMChannelOutOfBound;
    *pin->oc_register = 255-duty_cycle;//compensate for the inv.setting,
                                //makes non-inverting
    return PWMSuccess;
}

PWMError PWM_enable(uint8_t c, uint8_t enable){
    
    if( (((c == 5) | (c == 6))  & (Active_Timers[0] == 1)) |
        (((c == 9) | (c == 10)) & (Active_Timers[1] == 1)) |
        (((c == 11)| (c == 3))  & (Active_Timers[2] == 1))   ){ 
        return TimerActive;
    }
    
    if (c>=PINS_NUM)
        return PWMChannelOutOfBound;
    const Pin* pin = pins+c;
    if (!pin->tcc_register)
        return PWMChannelOutOfBound;
    *pin->oc_register=0;
    if (enable){
        *pin->tcc_register |= pin->com_mask;
        *pin->dir_register |= (1<<pin->bit);
    } else {
        *pin->tcc_register &= ~pin->com_mask;
        *pin->dir_register &= ~(1<<pin->bit);
    }
    return PWMSuccess;
}


PWMError PWM_isEnabled(uint8_t c) {
    
    if( (((c == 5) | (c == 6))  & (Active_Timers[0] == 1)) |
        (((c == 9) | (c == 10)) & (Active_Timers[1] == 1)) |
        (((c == 11)| (c == 3))  & (Active_Timers[2] == 1))   ){ 
        return TimerActive;
    }
    
    if (c>=PINS_NUM)
        return PWMChannelOutOfBound;
    const Pin* pin = pins+c;
    if (!pin->tcc_register)
        return PWMChannelOutOfBound;
    if ((*pin->tcc_register & pin->com_mask)==0)
        return 0;
    return PWMEnabled;
}

// detaches a timer from PWM mode. It will not disable pwm on channels.
void PWM_detach(char* timer){
    
    if     (!strcmp(timer, "timer_0")){ActivePWM[0] = 0;}
    else if(!strcmp(timer, "timer_1")){ActivePWM[1] = 0;}
    else if(!strcmp(timer, "timer_2")){ActivePWM[2] = 0;}
    return;
}
