#include <avr/io.h>
#include <avr/interrupt.h>
#include <string.h>

#include <flags.h>
uint8_t Active_Timers[3] = {0,0,0};
extern uint8_t ActivePWM[3];

// ArduinoUNO, ATmega 328p timers: timer 0, timer 1, timer 2.
// timer 0 is used on pin 5 and on pin 6.   It is 8 bit, with PWM.
// timer 1 is used on pin 9 and on pin 10.  It is 16 bit, with PWM.
// timer 2 is used on pin 11 and on pin 3.  It is 8 bit, with PWM and
//                                       with Asynchronous Operations.
// Here we hook all timers but only for OCnA

#include "timer.h"
// All three timers for the user
// we hook to timer 0, to timer 1 and to timer 2
#define NUM_TIMERS 3

typedef struct Timer{
  int timer_num;
  uint8_t prescaler;
  uint16_t int_freq;//(Hz)//used for timer 0 and timer 2 that are 8-bit
                        //and so can't have an int_freq of seconds order
  uint16_t duration_ms_16;  //used for timer 1 that is 16-bit
                        //and so can have an int_freq of seconds order
  uint8_t ocrval;
  TimerFn fn;
  void* args;
} Timer;

// NOTE:
// compare_match_register = [Fclk / (prescaler * desired_int_freq)] - 1

static Timer timers[NUM_TIMERS];

void Timers_init(void){
  memset(timers, 0, sizeof(timers));
  for (int i=0; i<NUM_TIMERS; ++i)
    timers[i].timer_num=i;
}

//FOR TIMER 1:
// creates a timer that has a duration of ms milliseconds
// bound to the device device
// each duration_ms the function timer_fn will be called
// with arguments timer args
//FOR TIMER 0 AND 2:
// creates a timer that has an interrupt frequency of int_freq
// bound to the device device
// each int_freq the function timer_fn will be called
// with arguments timer args
Timer* Timer_create(char* device,
            uint8_t prescaler,
            uint16_t int_freq,
            uint16_t duration_ms_16,
            TimerFn timer_fn,
            void* timer_args){
    Timer* timer = 0;
    if(!strcmp(device, "timer_0")){ 
        timer = timers;
        
        timer->prescaler = prescaler;
        timer->int_freq = int_freq;
        timer->ocrval = (F_CPU/(timer->prescaler * timer->int_freq))-1;
        timer->timer_num=0;
        timer->fn=timer_fn;
        timer->args=timer_args;
    }
    else if(!strcmp(device, "timer_1")){ 
        timer = &timers[1];
        
        timer->prescaler = prescaler;
        timer->duration_ms_16=duration_ms_16;
        timer->timer_num=1;
        timer->fn=timer_fn;
        timer->args=timer_args;
    }
    else if(!strcmp(device, "timer_2")){ 
        timer = &timers[2];
        
        timer->prescaler = prescaler;
        timer->int_freq = int_freq;
        timer->ocrval = (F_CPU/(timer->prescaler * timer->int_freq))-1;
        timer->timer_num=2;
        timer->fn=timer_fn;
        timer->args=timer_args;
    }
    else { return 0;}
    
    return timer;
}
    
// stops a timer
void Timer_stop(struct Timer* timer){
    
    if(timer->timer_num == 0){
        cli();
        TIMSK0 &= ~(1 << OCIE0A);
        sei();
    }
    else if(timer->timer_num == 1){
        cli();
        TIMSK1 &= ~(1 << OCIE1A);
        sei();
    }
    else if(timer->timer_num == 2){
        cli();
        TIMSK2 &= ~(1 << OCIE2A);
        sei();
    }
    
    return;
}

// stops and destroyes a timer
void Timer_destroy(struct Timer* timer){
  Timer_stop(timer);
  cli();
  int timer_num=timer->timer_num;
  memset(timer, 0, sizeof(Timer));
  timer->timer_num=timer_num;
  sei();
  
  //Set this timer as inactive
  Active_Timers[timer->timer_num] = 0;
}

void _timer0_start(struct Timer* timer){
  //Set this timer as active
  Active_Timers[timer->timer_num] = 1;
  
  TCCR0A = 0;
  TCCR0B = 0;
  OCR0A = timer->ocrval;
  TCCR0A |= (1 << WGM01);
  TCCR0B |= timer->prescaler;
  TIMSK0 |= (1 << OCIE0A);
}

void _timer1_start(struct Timer* timer){
  //Set this timer as active
  Active_Timers[timer->timer_num] = 1;
  
  uint16_t ocrval=(uint16_t)(15.62*timer->duration_ms_16); //15.62 to 
                                                    //have seconds order
  TCCR1A = 0;
  TCCR1B = 0;
  OCR1A = ocrval;
  TCCR1B |= (1 << WGM12);
  TCCR1B |= timer->prescaler;
  TIMSK1 |= (1 << OCIE1A);
}

void _timer2_start(struct Timer* timer){
  //Set this timer as active
  Active_Timers[timer->timer_num] = 1;
  
  TCCR2A = 0;
  TCCR2B = 0;
  OCR2A = timer->ocrval;
  TCCR2A |= (1 << WGM21);
  TCCR2B |= timer->prescaler;
  TIMSK2 |= (1 << OCIE2A);
}

// starts a timer
TimerError Timer_start(struct Timer* timer){
  //Controls if the timer is in use for PWM mode
  if(ActivePWM[timer->timer_num] == 1) {return TimerForPWM;}
  //Controls if timer's data are acceptable (for timer 0 and timer 2)
  if((((timer->timer_num == 0) | (timer->timer_num == 2)) &
    !((timer->ocrval >= 0) & (timer->ocrval <= 256))))
    {return InvalidOCRval;}
  
  if     (timer->timer_num==0){ cli(); _timer0_start(timer); sei();}
  else if(timer->timer_num==1){ cli(); _timer1_start(timer); sei();}
  else if(timer->timer_num==2){ cli(); _timer2_start(timer); sei();}
  
  return TimerSuccess;
}

ISR(TIMER0_COMPA_vect) {
  TCNT0 = 0;
  if(timers[0].fn)
    (*timers[0].fn)(timers[0].args);
}

ISR(TIMER1_COMPA_vect) {
  TCNT1 = 0;
  if(timers[1].fn)
    (*timers[1].fn)(timers[1].args);
}

ISR(TIMER2_COMPA_vect) {
  TCNT2 = 0;
  if(timers[2].fn)
    (*timers[2].fn)(timers[2].args);
}
        
