#pragma once
#include <stdint.h>

// initializes the ADC converter
void ADC_init(void);

// WARNING: it's assumed that analog pins can be used only as inputs.
// So during the initialization the direction will be set as input
// and it can't be get or set through specific functions.

// initializes the analog pins of the chip
void Analog_init(void);

// returns the number of analog pins on the chip
uint8_t Analog_numChannels(void);

// reads from an analog pin converted values through the ADC
uint16_t read_ADC(uint8_t apin);

// disables ADC
void ADC_end(void);



