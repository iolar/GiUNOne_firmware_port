#include <stdint.h>

typedef struct{
  volatile uint8_t* in_register;
  volatile uint8_t* out_register;
  volatile uint8_t* dir_register;
  uint8_t bit;   
  
  //mask for ADC multiplexer
  const uint8_t mux_mask;
    
} aPin;

#define ANALOG_PINS_NUM 6

extern const aPin apins[];
