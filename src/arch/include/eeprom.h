#pragma once
#include <stdint.h>

//WARNING: NOT good if a flash boot loader is present in the software!
//NOTE: good for software that implements access to EEPROM by interrupt!
//      In fact interrupts are controlled by disabling them globally.

//The EEPROM has an endurance of at least 100.000 write/erase cycles
//so use it wisely! (example: for configuration data)

//The EEPROM is 1KB (so addresses from 0 to 1023)

// initializes the eeprom subsystem
void EEPROM_init(void);

// reads in dest size bytes starting from eeprom location src
void EEPROM_read(void* dest, const uint16_t src, uint16_t size);

// writes in eeprom location dest size bytes starting from src
void EEPROM_write(uint16_t dest, const void* src,  uint16_t size);


