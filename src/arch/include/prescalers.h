#include <avr/io.h>

// Prescalers for TC0
#define prescalerTC0_1      (1 << CS00)
#define prescalerTC0_8      (1 << CS01)
#define prescalerTC0_64     (1 << CS01)|(1 << CS00)
#define prescalerTC0_256    (1 << CS02)
#define prescalerTC0_1024   (1 << CS02)|(1 << CS00)

// Prescalers for TC1
#define prescalerTC1_1      (1 << CS10)
#define prescalerTC1_8      (1 << CS11)
#define prescalerTC1_64     (1 << CS11)|(1 << CS10)
#define prescalerTC1_256    (1 << CS12)
#define prescalerTC1_1024   (1 << CS12)|(1 << CS10)

// Prescalers for TC2
#define prescalerTC2_1      (1 << CS20)
#define prescalerTC2_8      (1 << CS21)
#define prescalerTC2_32     (1 << CS21)|(1 << CS20)
#define prescalerTC2_64     (1 << CS22)
#define prescalerTC2_128    (1 << CS22)|(1 << CS20)
#define prescalerTC2_256    (1 << CS22)|(1 << CS21)
#define prescalerTC2_1024   (1 << CS22)|(1 << CS21)|(1 << CS20)
