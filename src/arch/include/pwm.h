#pragma once
#include <stdint.h>
#include <avr/io.h>

#define PWM_NUM 6

// -----------------------------------------------------//
// WARNING: Don't use active timers for PWM
//  and their related pins!
// Otherwise functions will return you an error.
// See below (MEMO) for timers-pins associations.
// -----------------------------------------------------//


typedef enum {
  PWMEnabled = 1,
  PWMSuccess = 0,
  PWMChannelOutOfBound = -1,
  PWMInvalidDutyCycle = -2,
  PWMInvalidPeriod = -3,
  TimerActive = -4 //"Stop and destroy this timer to use it"
} PWMError;

typedef enum{
    FastPWM = 0,
    PhaseCorrectPWM = 1
} PWMtype;

// ArduinoUNO, ATmega 328p timers: timer 0, timer 1, timer 2.
// timer 0 is used on pin 5 and on pin 6.   It is 8 bit, with PWM.
// timer 1 is used on pin 9 and on pin 10.  It is 16 bit, with PWM.
// timer 2 is used on pin 11 and on pin 3.  It is 8 bit, with PWM and
//                                       with Asynchronous Operations.

// initializes a timer for PWM mode
// and sets the PWM period to something reasonable
PWMError PWM_init(char* timer, PWMtype type, uint8_t prescaler);

// how many pwm on this chip?
uint8_t PWM_numChannels(void);

// what was the duty cycle I last set? negative value means error
uint8_t PWM_getDutyCycle(uint8_t pwm_channel);

// sets the duty cycle
PWMError PWM_setDutyCycle(uint8_t pwm_channel, uint8_t duty_cycle);

// enables (enable = 1) or disables (enable = 0) a pwm on a channel
PWMError PWM_enable(uint8_t num_channel, uint8_t enable);

// PWM enabled or not?
PWMError PWM_isEnabled(uint8_t num_channel);

// detaches a timer from PWM mode. It will not disable pwm on channels.
void PWM_detach(char* timer);
