#pragma once
#include <stdint.h>
struct Timer;

typedef void (*TimerFn)(void*);

// -----------------------------------------------------//
// WARNING: Don't use a timer that is used for PWM!
// Pay attenction to PWM pins!
// Otherwise functions will return you an error.
// See "timer.c" file for timers-pins associations.
// -----------------------------------------------------//

typedef enum{
    TimerSuccess = 0,
    TimerForPWM = -1, //"Detach this timer from PWM mode to use it"
    InvalidOCRval = -2 //"Adjust int_freq and/or prescaler"
    // NOTE:
    // compare_match_register = [Fclk/(prescaler * desired_int_freq)]-1
    // compare_match_register has to be included into timers' margins.
    //example: timer 0 and timer 2 (8-bit) go from 0 to 256;
    //  timer 1 (16-bit) go to 0 to 65536.
} TimerError;

//initializes the structures for the timers
void Timers_init(void);


// creates a timer that has a duration of ms milliseconds (for timer 1)
// creates a timer that has an interrupt frequency of int_freq 
// (timer 0 and timer 2).
// with a prescaler
// bound to the device device

//arduinUNO, ATmega328p: 3 devices. "timer_0"; "timer_1"; "timer_2";

// each duration_ms (for timer 1)
// or each int_freq (for timer 0 and timer 2)
// the function timer_fn will be called
// with arguments timer args
struct Timer* Timer_create(char* device,
                uint8_t prescaler,
                uint16_t int_freq, 
                uint16_t duration_ms_16, 
                TimerFn timer_fn, 
                void* timer_args);

// stops and destroyes a timer
void Timer_destroy(struct Timer* timer);

// starts a timer
TimerError Timer_start(struct Timer* timer);

// stops a timer
void Timer_stop(struct Timer* timer);
    
