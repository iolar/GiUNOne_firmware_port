#include "digio.h"
#include "timer.h"
#include "delay.h"
#include "uart.h"
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "prescalers.h"
#include <avr/interrupt.h>

struct UART* uart;
void printString(char* s){
    int l = strlen(s);
    for(int i = 0; i < l; i++, s++){
        UART_putChar(uart, (uint8_t) *s);
    }
}

uint16_t IR_freq = 38000;
char signal[64];
char tx[64];
uint8_t count = 0;
uint8_t pin = 11;
struct Timer* timer2;

void recIR(void* args){
    signal[count] = DigIO_getValue(pin);
    count = (count+1)%64;
}

int main(void){
    uart=UART_init("uart_0",115200);
    sprintf(tx, "Hi! I will receive data from IR receiver\n");
    printString(tx);
    
    DigIO_init();
    Timers_init();
    timer2 = Timer_create("timer_2", prescalerTC2_64, IR_freq, 0, recIR, (void*) NULL);
    Timer_start(timer2);
    sei();
        
    while(1){
        sprintf(tx,"capture: [");
        printString(tx);
        for(int i = 0; i < 64; i++){
            sprintf(tx," %d", signal[i]);
            printString(tx);
        }
        sprintf(tx,"]\n");
        printString(tx);
        delayMs(10);
    }
}
