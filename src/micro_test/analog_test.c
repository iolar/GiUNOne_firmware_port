#include "analog.h"
#include "uart.h"
#include "delay.h"
#include <stdio.h>
#include <string.h>

struct UART* uart;
void printString(char* s){
  int l=strlen(s);
  for(int i=0; i<l; ++i, ++s)
    UART_putChar(uart, (uint8_t) *s);
}

void printAnalogStatus(void){
    uint8_t num_channels = Analog_numChannels();
    char buffer[128];
    char* bend=buffer+sprintf(buffer, "Values=[");
    for (int i=0; i<num_channels; ++i)
        bend+=sprintf(bend,"%c%d: %d, ", 'A', i, read_ADC(i));
    bend+=sprintf(bend,"]\n");
    printString(buffer);
}

int main(void){
    
    ADC_init();
    Analog_init();
    
    uart = UART_init("uart_0", 115200);
    
    while(1){
        printAnalogStatus();
        delayMs(1000);
    }
    return 0;
}
