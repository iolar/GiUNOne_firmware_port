#include "analog.h"
#include "delay.h"
#include "encoder.h"
#include "prescalers.h"
#include "pwm.h"
#include "uart.h"
#include <stdint.h>
#include <stdio.h>
#include <string.h>

//__________________________________________________//
//Leds connected to pwm pins;
//Two Buttons connected for pull-up encoder(1) pins
//Potentiometer connected to analogic pin
//__________________________________________________//

uint16_t pot_value = 0; 
uint8_t  pot = 0;        //A0 - potentiometer
uint8_t  led[6] = {3,5,6,9,10,11};
uint16_t dc = 0;         //duty cycle

uint8_t encoder   = 1;
uint8_t prev_code = 0;
uint8_t new_code  = 0;

uint8_t prev_count = 0;
uint8_t new_count  = 0;

struct UART* uart;
char tx[64];

void printString(char* s){
    int l = strlen(s);
    for(int i = 0; i < l; i++, s++){
        UART_putChar(uart, (uint8_t) *s);
    }
}

int main(void){
    uart = UART_init("uart_0", 115200);
    sprintf(tx, "___Welcome! ^_^ \n");
    printString(tx);
    
    ADC_init();
    Analog_init();
    
    PWM_init("timer_0", FastPWM, prescalerTC0_1024);
    PWM_init("timer_1", FastPWM, prescalerTC1_1024);
    PWM_init("timer_2", FastPWM, prescalerTC2_1024);
    
    Encoder_init();
    
    while(1){            
        
        pot_value = read_ADC(pot);
        //pot_value : 1024 = duty_cycle : 64
        dc = (pot_value*64)/1024;
        
        Encoder_sample();
        new_code = Encoder_getValue(encoder);
        if(new_code > prev_code){
            PWM_enable(led[prev_count], 0);
            new_count = (prev_count+1)%PWM_NUM;
            PWM_enable(led[new_count], 1);
        }
        
        PWM_setDutyCycle(led[new_count], (uint8_t) dc);
        
        sprintf(tx, "\n pot_value: %d; duty_cycle: %d; code: %d; count: %d; pin: %d\n",pot_value,dc,new_code,new_count,led[new_count]);
        printString(tx);
        
        prev_count = new_count;
        prev_code = new_code;
        
        delayMs(50);
    }
} 
    
