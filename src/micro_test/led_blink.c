#include "digio.h"
#include "delay.h"
#include <stdint.h>

int main(void){
    
    uint8_t pin = 13;
    DigIO_init();
    DigIO_setDirection(pin,1);
    
    while(1){
        DigIO_setValue(pin,1);
        delayMs(500);
        
        DigIO_setValue(pin,0);
        delayMs(500);
    }
}
