#include "timer.h"
#include "uart.h"
#include "delay.h"
#include <stdio.h>
#include <string.h>
#include "prescalers.h"

struct UART* uart;
void printString(char* s){
  int l=strlen(s);
  for(int i=0; i<l; ++i, ++s)
    UART_putChar(uart, (uint8_t) *s);
}

void timerFn(void* args){
  uint16_t* argint=(uint16_t*)args;
  *argint=1;
}

int main(void){
  uart=UART_init("uart_0",115200);
  Timers_init();
  volatile uint16_t do_stuff0;
  volatile uint16_t do_stuff1;
  volatile uint16_t do_stuff2;
  struct Timer* timer0=Timer_create("timer_0", prescalerTC0_64, 38000, 0, timerFn, (void*) &do_stuff0);
  struct Timer* timer1=Timer_create("timer_1", prescalerTC1_1024, 0, 1000, timerFn, (void*) &do_stuff1);
  struct Timer* timer2=Timer_create("timer_2", prescalerTC2_64, 38000, 0, timerFn, (void*) &do_stuff2);
  Timer_start(timer0);
  Timer_start(timer1);
  Timer_start(timer2);
  uint16_t tick0=0;
  uint16_t tick1=0;
  uint16_t tick2=0;
  while(1) {
    if (do_stuff0){
      char buffer0[20];
      ++tick0;
      sprintf(buffer0, "Tick0: [ %05d]\n ", tick0);
      printString(buffer0);
      do_stuff0=0;
    }
    if (do_stuff1){
      char buffer1[20];
      ++tick1;
      sprintf(buffer1, "Tick1: [ %05d]\n ", tick1);
      printString(buffer1);
      do_stuff1=0;
    }
    if (do_stuff2){
      char buffer2[20];
      ++tick2;
      sprintf(buffer2, "Tick2: [ %05d]\n ", tick2);
      printString(buffer2);
      do_stuff2=0;
    }
  }
}
